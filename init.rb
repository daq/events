require "sequel"
require "sinatra"
require "slim"
require "mysql2"
require "unicode_utils"
require "omniauth"
require "omniauth-twitter"
#require "logger"
require "redcarpet"
require 'time'
require "json"
require "digest/md5"

configure :development do
  require "open-uri"
  DB = Sequel.connect("mysql2://root:root@localhost:3306/events_dev", :encoding => "UTF8", :loggers => [Logger.new($stdout)])
  use OmniAuth::Builder do
    provider :twitter, "kVT21SRy272jjWaJom5bA", "88HSmorc9879rlECnQBQYzQ697ef4xjR7seoJt9CM"
  end
  Slim::Engine.set_default_options :pretty => true
  #OmniAuth.config.test_mode = true
end

configure :production do
  DB = Sequel.connect("mysql2://ai_eventful:1ns3y3d1@localhost:3306/ai_eventful", :encoding => "UTF8")
  use OmniAuth::Builder do
    provider :twitter, "yet9ZJMKvHei05hADI4w", "y0wSazJfGeYYhXzTxRKyiBFMKCcyqsTmTzc4ZXkgTQ"
  end
end

use Rack::Session::Cookie, :expire_after => 2592000, :secret => "ac4ed982d0bb14d5" # OmniAuth requires sessions
Sequel::Model.plugin :timestamps
Sequel.extension :pagination

if defined? Encoding
  Encoding.default_external = Encoding::UTF_8
  Encoding.default_internal = Encoding::UTF_8
end

set :root, File.dirname(__FILE__)
set :app_file, __FILE__
set :default_encoding, 'utf-8'
set :sessions, true
set :static, true
set :default_language, "en"
set :languages, ["en", "ru"]
#set :imgur_key, '646f943bc19329651ae04e0fd9b5d69a'

# Load all application files
Dir[File.join(File.dirname(__FILE__), "app", "**", "*.rb")].each do |file|
  require file
end

Sequel.migration do
    up do
        create_table :users do
            primary_key	:id
            Integer     :uid
            String      :nickname
           	String		:name
            String      :provider, :default => "twitter"
            String      :image, :default => "/img/avatar.gif"
            String      :language
            String      :email
            Timestamp   :created_at
        end

        create_table :events do
            primary_key :id
            String      :title
            Text		:description
            Text		:description_html
            Integer 	:author
            String      :city
            String      :short_url
            String      :country
            Float       :latitude
            Float       :longitude
            Timestamp   :servertime
            Timestamp   :starts
            Boolean     :is_private, :default => false
            Timestamp	:created_at
            Timestamp	:updated_at
        end

        create_table :tags do
            primary_key :id
            String      :tag_name
            Timestamp   :created_at
            Timestamp   :updated_at
        end

        create_table :events_tags do
            primary_key :id
            foreign_key :event_id
            foreign_key :tag_id
        end

        create_table :events_users do
            primary_key :id
            foreign_key :event_id
            foreign_key :user_id
        end

        create_table :notifications do
            primary_key :id
            foreign_key :user_id
            Integer     :notifier_id
            foreign_key :event_id
            Integer     :type # 0 => inactive, 1 => invitation, 2 => user applied, 3 => user deleted
        end
=begin
        create_table :tiles do
            primary_key :id
            String      :title
            String      :type
            String      :url
            String      :thumbnail_url
            String      :delete_url
            String      :inner_id
            String      :description
            String      :description_html
            foreign_key :event_id
            foreign_key :user_id
            Timestamp   :created_at
            Timestamp   :updated_at
        end

        create_table :photos do
            primary_key :id
            String      :url
            String      :thumbnail_url
            String      :title
            foreign_key :tile_id
            Timestamp   :created_at
        end

        create_table :comments do
            primary_key :id
            Text        :comment
            Text        :comment_html
            foreign_key :tile_id
            foreign_key :user_id
            Timestamp   :created_at
            Timestamp   :updated_at
        end
=end
    end

    down do
        drop_table      :users
        drop_table      :events
        drop_table      :tags
        drop_table      :events_tags
        drop_table      :events_users
        drop_table      :notifications
=begin
        drop_table      :tiles
        drop_table      :photos
        drop_table      :comments
=end
    end

end
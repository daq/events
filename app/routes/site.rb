# coding: utf-8

get '/' do
  redirect '/timeline'
end

get '/event/new' do
  if current_user
    @notifications = Notification.filter({:user_id => current_user.id, :type => 1..3}).count
    slim :new
  else
    redirect '/timeline'
  end
end

get '/event/:eventid' do
  if current_user
    @notifications = Notification.filter({:user_id => current_user.id, :type => 1..3}).count
  end
  @event = Event.filter(:id => params["eventid"]).first
  slim :show_event
end

get '/event/:eventid/delete' do
  @event = Event.filter(:id => params["eventid"]).first
  if @event && current_user && current_user.id == @event.author
    Event.filter(:id => params["eventid"]).delete
  end
  redirect '/timeline'
end

get '/user/:user_id' do
  if current_user
    @notifications = Notification.filter({:user_id => current_user.id, :type => 1..3}).count
  end
  slim :index
end

get '/map' do
  if current_user
    @notifications = Notification.filter({:user_id => current_user.id, :type => 1..3}).count
  end
  slim :event_map
end

get '/map/:eventid' do
  if current_user
    @notifications = Notification.filter({:user_id => current_user.id, :type => 1..3}).count
  end
  @event = Event.filter(:id => params["eventid"]).first
  slim :event_map
end

get '/settings' do
    if current_user
      @notifications = Notification.filter({:user_id => current_user.id, :type => 1..3}).count
      slim :settings
    else
      redirect '/timeline'
    end
  #end
end

get '/tag/:mytag' do
  if current_user
    @notifications = Notification.filter({:user_id => current_user.id, :type => 1..3}).count
  end
  slim :index
end

['/timeline', '/my_events', '/top_visitors'].each do |link|
  get link do
    if current_user
      @notifications = Notification.filter({:user_id => current_user.id, :type => 1..3}).count
      p current_user.notifications
    end
    @message = get_last_message
    slim :index
  end
end

post '/ajx' do
  data = JSON.parse(params.keys[0])
  links = ["timeline",
    "top_visitors",
    "top_questions",
    "my_events",
    "user_events"
  ]
  begin
    #p data
    raise "bad data" if ["page", "feature"].any? {|param| data[param].nil? || data[param] == ""}
    page = data['page']
    feature = data['feature']
    raise "no such feature" unless links.any? {|link| feature == link}
    raise "you should log in" if ((not current_user) && feature == "my_events")
    raise "no user specified" if (feature == "user_events" && (data['user_id'].nil? || data['user_id'] == ""))
    tagged = false
    limits = 20
    unless data['tag'].nil? || data['tag'] == ""
      @tag = Tag.filter(:tag_name => data['tag']).first
      tagged = true unless @tag.nil?
    end
    @events_dataset = nil
    tn = Time.now.utc
    case feature
      when "timeline"
        if tagged
          @events_dataset = @tag.events_dataset.filter('servertime > ?', tn).order(:servertime)#.reverse_order(:is_private)
        else
          @events_dataset = Event.filter('servertime > ?', tn).order(:servertime)#.reverse_order(:is_private)
        end
        @events = @events_dataset.paginate(page, limits)
        puts "timeline"
      when "top_visitors"
        @events_array = DB[:events_users].group_and_count(:event_id).order(:count.desc).all
        @events = @events_array.map {|ec| Event.filter(:id => ec[:event_id]).filter('servertime > ?', tn).first}#.sort_by{|x| x.is_private.to_s }.reverse
        @events = @events[((page-1)*limits)..(page*limits)]
        #@events = @events_dataset.paginate(page, limits)
        puts "top_visitors"
      when "top_questions"
        @events_array = DB[:tiles].group_and_count(:event_id).order(:count.desc).all
        @events = @events_array.map {|ec| Event.filter(:id => ec[:event_id]).filter('servertime > ?', tn).first}
        @events = @events[((page-1)*limits)..(page*limits)]
        #@events = @events_dataset.paginate(page, limits)
        puts "top_media"
      when "my_events"
        @events = Event.filter(:author => current_user.id).order(:created_at.desc)
      when "user_events"
        @events = Event.filter(:author => data['user_id']).order(:created_at.desc)
      else
        puts "bad data"
        @events_dataset = @events = nil
    end
  rescue Exception => e
    puts e.message
  end
  unless @events.nil?
    content_type 'text/html', :charset => 'utf-8'
    slim :events, :layout => false
  end

end

post '/feature' do
  reply = {}
  features = ["add_user",
    "remove_user"
  ]
  getters = [
  ]
  user_count = 20
  data = JSON.parse(params.keys[0])
  begin
    # check parameters
    raise "bad data" if ["event_id", "feature"].any? {|param| data[param].nil? || data[param] == ""}
    @event = Event.filter(:id => data["event_id"]).first
    raise "no such event" if @event.nil?
    feature = data["feature"]
    raise "no such feature" unless (features+getters).any? {|f| feature == f}
    raise "you should log in" unless (current_user && features.any?{|f| feature == f} || getters.any?{|f| feature == f})
    case feature
    when "add_user"
      raise "user already included" if @event.user_dataset.include?(current_user)
      @event.add_user(current_user)
      @notification = Notification.create(:user_id => current_user.id,
        :notifier_id => @event.author,
        :event_id => @event.id,
        :type => 2
        )
    when "remove_user"
      raise "author cannot exclude himself" if @event.author == @current_user.id
      raise "no such user included" unless @event.user_dataset.include?(current_user)
      @event.remove_user(current_user)
      @notification = Notification.create(:user_id => current_user.id,
        :notifier_id => @event.author,
        :event_id => @event.id,
        :type => 3
        )
    else
      raise "no such feature: "+feature
    end
  rescue Exception => e
    reply = {:error => e.message}
  end
  content_type :json
  reply.to_json
end

post '/' do
  unless params.keys.size == 0
    data = {}
    reply = {}
    begin
      data = JSON.parse(params.keys[0])
    rescue JSON::ParserError => e
      reply = {'errors' => [{:id => :json, :text => "json parse error"}]}
      return reply.to_json
    end
    @event = Event.filter(:id => data['event_id']).first unless data['event_id'].nil? || data['event_id'] == ''
    begin
      raise "you should log in" unless current_user
      raise "no such feature" unless ['add', 'update'].include?(data['feature'])
      errors = []
      errors.push({:id => :title, :text => "wrong or too large title"}) if bad_string(data['title'])
      errors.push({:id => :city, :text => "wrong or too large city name"}) if bad_string(data['city'])
      errors.push({:id => :country, :text => "wrong or too large country name"}) if bad_string(data['country'])
      errors.push({:id => :description, :text => "wrong or too large description"}) if bad_text(data['description'])
      errors.push({:id => :latitude, :text => "wrong latitude"}) if bad_int(data['latitude'])
      errors.push({:id => :longitude, :text => "wrong longitude"}) if bad_int(data['longitude'])
      p errors
      raise "errors" if errors.size > 0
      if @event.nil? && data['feature'] == 'add'
        @event = Event.create(:title => data['title'],
          :description => data["description"],
          :description_html => to_html(data["description"]),
          :author => session[:user_id],
          :city => data['city'],
          :country => data['country'],
          :latitude => data['latitude'],
          :longitude => data['longitude'],
          :servertime => get_server_time(data['date'], data['time'], data['gmtoffset']),
          :is_private => "false",
          :starts => get_local_time(data['date'], data['time']),
          :created_at => Time.now,
          :updated_at => Time.now)
        current_user.add_event(@event)
        get_tags(data["tags"]).each { |tag|
          t = Tag[:tag_name => tag]
          if t.nil?
            t = Tag.create( :tag_name => tag,
              :created_at => Time.now,
              :updated_at => Time.now)
          end
          t.add_event(@event)
        }
        reply = {:id => @event.id}
      elsif (not @event.nil?) and current_user.id == @event.author && data['feature'] == 'update'
        @event.update(
          :title => data['title'],
          :description => data["description"],
          :description_html => to_html(data["description"]),
          :city => data['city'],
          :latitude => data['latitude'],
          :longitude => data['longitude'],
          :servertime => get_server_time(data['date'], data['time'], data['gmtoffset']),
          :starts => get_local_time(data['date'], data['time']),
          :updated_at => Time.now)
        @event.remove_all_tags
        get_tags(data["tags"]).each { |tag|
          t = Tag[:tag_name => tag]
          if t.nil?
            t = Tag.create( :tag_name => tag,
              :created_at => Time.now,
              :updated_at => Time.now)
          end
          t.add_event(@event)
        }
        reply = {:id => @event.id}
      end
    rescue Exception => e
      p e.message
      reply = {'errors' => errors}
    end
    return reply.to_json
  #redirect '/timeline'
  end
end

get '/auth/:provider/callback' do
  @auth = request.env['omniauth.auth']
  image = ""
  image = (@auth["info"]["image"].nil? || @auth["info"]["image"] == "") ? url('img/avatar.gif') : @auth["info"]["image"]
  usr = User.find_or_create(:uid => @auth["uid"],
    :nickname => @auth["info"]["nickname"],
    :name => @auth["info"]["name"],
    :provider => "twitter",
    :language => "en",
    :image => image)
  session[:user_id] = usr.id
  redirect '/settings'
  #redirect request.env['omniauth.origin'] || '/'
end

post '/settings' do
  unless params.keys.size == 0
    data = JSON.parse(params.keys[0])
    errors = []
    begin
      raise "you should log in" unless current_user
      errors.push({:id => :email, :text => "no email specified"}) if data['email'].nil? || data['email'] == ""
      email = data['email']
      errors.push({:id => :email, :text => "wrong email format"}) if /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/.match(email).nil?
      errors.push({:id => :lang, :text => "wrong language specified"}) if data['lang'].nil? || !settings.languages.include?(data['lang'])
      lang = data['lang']
      usr = User.filter(:id => current_user.id).update(:email => email, :language => lang) if errors.size == 0
      reply = {:id => current_user.id}
    rescue Exception => e
      p e.message
      reply = {'errors' => errors}
    end
    return reply.to_json
  end
end

get '/auth/logout' do
  session.clear
  redirect '/timeline'  
end

get '/*' do
  redirect '/timeline'
end
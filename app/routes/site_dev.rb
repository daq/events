# coding: utf-8

get '/event/:event_id/edit' do
  if current_user
    slim :settings
  end
  @event = Event.filter(:id => params["event_id"]).first
  if current_user && @event.author == @current_user.id
    slim :new
  else
    redirect '/timeline'
  end
end

get '/calendar' do
  if current_user
    @notifications = Notification.filter({:user_id => current_user.id, :type => 1..3}).count
  end
  slim :calendar
end

post '/feature_dev' do
  reply = {}
  features = [
  ]
  getters = [
    "get_events_by_coords",
    "get_events_i_joined",
    "get_all_events",
    "get_closest_events",
    "get_suggested_events",
  ]
  user_count = 20
  p params
  data = JSON.parse(params.keys[0])
  begin
    # check parameters
    #raise "bad data" if ["event_id", "feature"].any? {|param| data[param].nil? || data[param] == ""}
    raise "bad data" if data["feature"].nil? || data["feature"] == ""
    #@event = Event.filter(:id => data["event_id"]).first
    #raise "no such event" if @event.nil?
    feature = data["feature"]
    raise "no such feature" unless (features+getters).any? {|f| feature == f}
    case feature
    when "get_user_events"
      raise "you should log in" unless current_user
      raise "no year specified" unless data["year"]
      year = data["year"].to_i
      raise "no month specified" unless data["month"]
      month = data["month"].to_i
      raise "no day specified" unless data["day"]
      day = data["day"]
      next_m = month == 11 ? 0 : month + 1
      next_y = month == 11 ? year+1 : year

      @events = current_user.events_dataset.filter(:starts => Date.new(year, month, 1)...Date.new(next_y, next_m, 1))
      #events_current = current_user.events_dataset.filter(:starts => Date.new(year, month, 1)..Date.new(year, month+1, 1).prev_day)
      #events_next = current_user.events_dataset.filter(:starts => Date.new(year, month+1, 1)..Date.new(year, month+2, 1).prev_day)
      reply = @events.map { |ev|
        {
          :id => ev.id,
          :title => ev.title,
          :month => ev.starts.month,
          :day => ev.starts.day
        }
      }
    when "get_events_by_coords"
      raise "no coords given" if (data["ul_lat"].nil? || data["ul_lat"] == "") || (data["ul_lng"].nil? || data["ul_lng"] == "") || (data["lr_lat"].nil? || data["lr_lat"] == "") || (data["lr_lng"].nil? || data["lr_lng"] == "")
      upper_left = [data["ul_lat"], data["ul_lng"]]
      lower_right = [data["lr_lat"], data["lr_lng"]]
      @events = Event.filter({:latitude => upper_left[0]..lower_right[0], :longitude => upper_left[1]..lower_right[1]})
      reply = @events.map { |ev|
        {
          :id => ev.id,
          :title => ev.title,
          :latitude => ev.latitude,
          :longitude => ev.longitude,
          :city => ev.city,
          :starts => ev.starts,
          :people => ev.user_dataset.count
        }
      }
    when "get_events_i_joined"
      raise "you should log in" unless current_user
      @events = current_user.events_dataset.filter('servertime > ?', Time.now.utc)
      reply = @events.map { |ev|
        {
          :id => ev.id,
          :title => ev.title,
          :latitude => ev.latitude,
          :longitude => ev.longitude,
          :city => ev.city,
          :starts => ev.starts,
          :people => ev.user_dataset.count
        }
      }
    when "get_closest_events"
      #raise "you should log in" unless current_user
      raise "no user coordinates" if ((data["lat"].nil? || data["lat"] == "") || (data["lng"].nil? || data["lng"] == ""))
      user_lat = data["lat"]
      user_lng = data["lng"]
      @events = current_user.events_dataset.filter('servertime > ?', Time.now.utc).filter({:latitude => (user_lat-5)..(user_lat+5), :longitude => (user_lng-5)..(user_lng+5)})
      reply = @events.map { |ev|
        {
          :id => ev.id,
          :title => ev.title,
          :latitude => ev.latitude,
          :longitude => ev.longitude,
          :city => ev.city,
          :starts => ev.starts,
          :people => ev.user_dataset.count
        }
      }
    when "get_all_events"
      @events = Event.filter('servertime > ?', Time.now.utc)
      reply = @events.map { |ev|
        {
          :id => ev.id,
          :title => ev.title,
          :latitude => ev.latitude,
          :longitude => ev.longitude,
          :city => ev.city,
          :starts => ev.starts,
          :people => ev.user_dataset.count
        }
      }
    when "get_suggested_events"
      raise "no event specified" if data['event_id'].nil? || data['event_id'] == ""
      @event = Event.filter(:id => data['event_id']).first
      raise "no such event" if @event.nil?
      @events = Event.filter('(servertime > ?) and (id != ?)', Time.now.utc, @event.id).order(rand).limit(3)
      reply = @events.map { |ev|
        {
          :id => ev.id,
          :title => ev.title,
          :latitude => ev.latitude,
          :longitude => ev.longitude,
          :city => ev.city,
          :starts => ev.starts,
          :people => ev.user_dataset.count
        }
      }
    else
      raise "no such feature: "+feature
    end
  rescue Exception => e
    reply = {:error => e.message}
  end
  content_type :json
  reply.to_json
end

post '/post_files' do
  event_id = params['event_id']
  event = Event.filter(:id => event_id).first
  reply = nil
  unless event.nil?
    file = params['files'][0]
    orig_link, delete_link, square_th, large_th = local_store(file[:tempfile].to_path, file[:filename])
    photo = Photo.find_or_create(:url => orig_link,
      :thumbnail_url => square_th,
      :event_id => event.id,
      :user_id => current_user.id)
    reply = [
      {
      "name" => file[:filename],
      "size" => file[:tempfile].size,
      "url" => orig_link,
      "thumbnail_url" => square_th,
      "delete_url" => delete_link,
      "delete_type" => "GET"
      }
    ]
  else
    reply = []
  end
  content_type :json
  reply.to_json
end


class User < Sequel::Model
	plugin :validation_helpers
  many_to_many	:events
  one_to_many  :notifications
  
  def validate
    validates_presence [:uid, :nickname, :name,  :provider, :image]
    #validates_unique([:uid, :provider])
    #validates_unique([:uid, :nickname])
    validates_format /twitter/, :provider
    validates_format /https?:\/\/[\w\d\-\.\/]+\.(jpg|jpeg|gif|png)/, :image
    validates_max_length 255, :nickname
    validates_max_length 255, :name
    validates_max_length 255, :provider
  end
end
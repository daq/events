class Event < Sequel::Model
	plugin :validation_helpers
  many_to_many   :user
	many_to_many   :tags
  many_to_many  :notifications

  def validate
    validates_presence [:title, :description, :description_html, :starts, :city, :author, :servertime, :latitude, :longitude, :country]
    #validates_unique ([:description, :starts, :author])
    validates_max_length 20480, :description
    validates_max_length 20480, :description_html
    validates_max_length 255, :city
    validates_max_length 255, :country
    validates_max_length 255, :title
  end
end
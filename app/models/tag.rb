class Tag < Sequel::Model
	plugin :validation_helpers
  many_to_many		:events

  def validate
    validates_presence :tag_name
    validates_unique :tag_name
    validates_max_length 255, :tag_name
  end
end
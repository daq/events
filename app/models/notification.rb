class Notification < Sequel::Model
  plugin :validation_helpers
  many_to_one   :user
  many_to_many   :events

  def validate
    validates_presence [:user_id, :notifier_id, :type]
  end
end
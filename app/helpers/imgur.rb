require 'net/http'
require 'uri'
require 'base64'
#require "mini_magick"

module Images

  include FileUtils::Verbose
  
  def resize_and_crop(image, size)
    if image[:width] < image[:height]
      remove = ((image[:height] - image[:width])/2).round
      image.shave("0x#{remove}")
    elsif image[:width] > image[:height]
      remove = ((image[:width] - image[:height])/2).round
      image.shave("#{remove}x0")
    end
    image.resize("#{size}x#{size}")
    return image
 end

  def imgur(imagefile)
    key = "646f943bc19329651ae04e0fd9b5d69a"

    tmpfile = imagefile
    imagedata = Base64.encode64(File.read(tmpfile))

    Net::HTTP::Proxy(nil, nil).start('api.imgur.com',80) { |http|
      res = Net::HTTP.post_form(URI.parse('http://api.imgur.com/2/upload.json'),{'image' => imagedata, 'key' => key})
      json_data = res.body
      data = JSON.parse(json_data)['links']
      return [data['original'], data['delete'], data['small_square'], data['large_thumbanail']]
    }
  end

  def local_store(imagefile, filename)
    fbase = Digest::MD5.hexdigest(filename)
    fname = fbase + '.' + filename.split('.')[-1]
    fname_del = fbase + '_del'
    fname_th = fbase + '_th'
    thumbnail = resize_and_crop(MiniMagick::Image.open(imagefile), 90).write("public/images/#{fname_th}")
    cp(imagefile, "public/images/#{fname}")
    return ["images/#{fname}", "#{fname_del}", "images/#{fname_th}", ""]

  end

  class Imager

    attr_reader :imagefile, :thumbnail, :del_image

    def initialize(filename, tempfile)
      @filename, @tempfile = filename, tempfile
      save_image
    end

    def save_image
      fbase = Digest::MD5.hexdigest(@filename)
      filedir = fbase[0...2]
      image_base = "public/images/#{filedir}/#{fbase}"
      fname_th = fbase + '_th' + '.' + filename.split('.')[-1]
      image_rel = image_base + '.' + @filename.split('.')[-1]
      thumb_rel = image_base + '_th.' + @filename.split('.')[-1]
      del_rel = image_base + "_del"
      
      @imagefile = url(image_rel)
      @thumbnail = url(thumb_rel)
      @del_image = url(del_rel)
      
      resize_and_crop(MiniMagick::Image.open(@tempfile), 90).write(thumb_rel)
      cp(@tempfile, image_rel)
    end

  end

end

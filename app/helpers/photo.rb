module PhotoHelpers

  def get_image_thumbnail(photo)
    re = /i\.(imgur)\.com\//
    if re.match(photo)[1] == "imgur"
      return photo.gsub(/(\.jpg|\.png|\.gif)$/, 's\1')
    else
      return nil
    end
  end

  def add_photos(pd)
    pd.map{|photo|
      "<a rel='gallery' href='#{url(photo.url)}'><img src='#{url(photo.thumbnail_url)}' /></a>"
    }.join
  end

end
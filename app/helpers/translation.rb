# -*- coding: utf-8 -*-

module Translation

  def t(name)
    tr = {
      "soonest" => {
        "en" => "Soonest",
        "ru" => "Ближайшие"
      },
      "popular" => {
        "en" => "Popular",
        "ru" => "Популярные"
      },
      "event_map" => {
        "en" => "Event map",
        "ru" => "Карта событий"
      },
      "add_event" => {
        "en" => "Add new event",
        "ru" => "Добавить событие"
      },
      "notifications" => {
        "en" => "Notifications",
        "ru" => "Уведомления"
      },
      "join_with" => {
        "en" => "Join using",
        "ru" => "Войти с помощью"
      },
      "my_events" => {
        "en" => "My events",
        "ru" => "Мои события"
      },
      "settings" => {
        "en" => "Settings",
        "ru" => "Настройки"
      },
      "logout" => {
        "en" => "Logout",
        "ru" => "Выход"
      },
      "share_event" => {
        "en" => "Share this event",
        "ru" => "Расскажите друзьям"
      },
      "edit_event" => {
        "en" => "Edit this event",
        "ru" => "Редактировать"
      },
      "join" => {
        "en" => "Join",
        "ru" => "Присоединиться"
      },
      "quit" => {
        "en" => "Quit",
        "ru" => "Отказаться"
      },
      "find_me" => {
        "en" => "Show my location",
        "ru" => "Найти меня"
      },
      "event_calendar" => {
        "en" => "Event calendar",
        "ru" => "Календарь событий"
      },
      "where_happen" => {
        "en" => "Event location",
        "ru" => "Где все произойдет?"
      },
      "interesting" => {
        "en" => "You might also like",
        "ru" => "Вас может заинтересовать"
      },
      "add_new_event" => {
        "en" => "Add new event",
        "ru" => "Добавить новое событие"
      },
      "add_title" => {
        "en" => "Funny or boring formal event title",
        "ru" => "Оригинальное или скучное официальное название"
      },
      "add_place" => {
        "en" => "Where will all the stuff happen?",
        "ru" => "Где произойдет все самое интересное?"
      },
      "insert_place" => {
        "en" => "Enter location",
        "ru" => "Введите название места"
      },
      "find_on_map" => {
        "en" => "Find on the map",
        "ru" => "Найти на карте"
      },
      "add_when" => {
        "en" => "When should we arrive?",
        "ru" => "Когда надо быть на месте?"
      },
      "add_description" => {
        "en" => "Description, program, manifesto - tell us more!",
        "ru" => "Описание, программа, манифест - расскажите, что нас ждет!"
      },
      "markdown" => {
        "en" => "* You may use Markdown language for styling",
        "ru" => "* Вы можете использовать язык разметки Markdown"
      },
      "add_tags" => {
        "en" => "Don't forget to add some tags",
        "ru" => "Осталось только добавить теги"
      },
      "save_event" => {
        "en" => "Publish",
        "ru" => "Опубликовать"
      },
      "no_event" => {
        "en" => "No such event",
        "ru" => "Нет такого события"
      },
      #--------
      "login_to" => {
        "en" => "Login to",
        "ru" => "Войдите, чтобы"
      },
      "the_event" => {
        "en" => "the event",
        "ru" => "к событию"
      },
      #-------
      "all" => {
        "en" => "All",
        "ru" => "Все"
      },
      "nearest_to_me" => {
        "en" => "Nearest to me",
        "ru" => "Ближайшие ко мне"
      },
      "i_joined" => {
        "en" => "Events I joined",
        "ru" => "Те, где я участвую"
      },
      "go" => {
        "en" => "joined",
        "ru" => "идут"
      },
      "at" => {
        "en" => "at",
        "ru" => "ждем"
      },
      "ago" => {
        "en" => "ago",
        "ru" => "назад"
      },
      "save" => {
        "en" => "Save",
        "ru" => "Сохранить"
      },
      "1minute" => {
        "en" => "1 minute",
        "ru" => "1 минута"
      },
      "minutes" => {
        "en" => " minutes",
        "ru" => " минут"
      },
      "hours" => {
        "en" => " hours",
        "ru" => " часов"
      },
      "days" => {
        "en" => " days",
        "ru" => " дней"
      },
      "person" => {
        "en" => "person",
        "ru" => "человек"
      },
      "people" => {
        "en" => "people",
        "ru" => "человек"
      },
      "language" => {
        "en" => "Language",
        "ru" => "Язык"
      },
      "January" => {
        "en" => "January",
        "ru" => "января"
      },
      "February" => {
        "en" => "February",
        "ru" => "февраля"
      },
      "March" => {
        "en" => "March",
        "ru" => "марта"
      },
      "April" => {
        "en" => "April",
        "ru" => "апреля"
      },
      "May" => {
        "en" => "May",
        "ru" => "мая"
      },
      "June" => {
        "en" => "June",
        "ru" => "июня"
      },
      "July" => {
        "en" => "July",
        "ru" => "июля"
      },
      "August" => {
        "en" => "August",
        "ru" => "августа"
      },
      "September" => {
        "en" => "September",
        "ru" => "сентября"
      },
      "October" => {
        "en" => "October",
        "ru" => "октября"
      },
      "November" => {
        "en" => "November",
        "ru" => "ноября"
      },
      "December" => {
        "en" => "December",
        "ru" => "декабря"
      },
      "no notifications" => {
        "en" => "You have no notifications",
        "ru" => "У вас нет непросмотренных уведомлений"
      }
    }
    tr.key?(name) ? tr[name][default_lang] : " ----- missing ----- "
  end

  def ttime(type, number)
    if [:minute, :hour, :day, :month, :year, :person].include?(type)
      {
      0 => {
        :minute => {
          "en" => " minutes",
          "ru" => " минут"
          },
        :hour => {
          "en" => " hours",
          "ru" => " часов"
          },
        :day => {
          "en" => " days",
          "ru" => " дней"
          },
        :month => {
          "en" => " months",
          "ru" => " месяцев"
          },
        :year => {
          "en" => " years",
          "ru" => " лет"
          },
        :person => {
          "en" => " people",
          "ru" => " человек"
          }
        },
      1 => {
        :minute => {
          "en" => " minutes",
          "ru" => " минуту"
          },
        :hour => {
          "en" => " hours",
          "ru"  => " час"
          },
        :day => {
          "en" => " days",
          "ru" => " день"
          },
        :month => {
          "en" => " months",
          "ru" => " месяц"
          },
        :year => {
          "en" => " years",
          "ru" => " год"
          },
        :person => {
          "en" => " people",
          "ru" => " человек"
          }
        },
      2 => {
        :minute => {
          "en" => " minutes",
          "ru" => " минуты"
          },
        :hour => {
          "en" => " hours",
          "ru"  => " часа"
          },
        :day => {
          "en" => " days",
          "ru" => " дня"
          },
        :month => {
          "en" => " months",
          "ru" => " месяца"
          },
        :year => {
          "en" => " years",
          "ru" => " года"
          },
        :person => {
          "en" => " people",
          "ru" => " человека"
          }
        },
      3 => {
        :minute => {
          "en" => " minutes",
          "ru" => " минуты"
          },
        :hour => {
          "en" => " hours",
          "ru"  => " часа"
          },
        :day => {
          "en" => " days",
          "ru" => " дня"
          },
        :month => {
          "en" => " months",
          "ru" => " месяца"
          },
        :year => {
          "en" => " years",
          "ru" => " года"
          },
        :person => {
          "en" => " people",
          "ru" => " человека"
          }
        },
      4 => {
        :minute => {
          "en" => " minutes",
          "ru" => " минуты"
          },
        :hour => {
          "en" => " hours",
          "ru"  => " часа"
          },
        :day => {
          "en" => " days",
          "ru" => " дня"
          },
        :month => {
          "en" => " months",
          "ru" => " месяца"
          },
        :year => {
          "en" => " years",
          "ru" => " года"
          },
        :person => {
          "en" => " people",
          "ru" => " человека"
          }
        },
      5 => {
        :minute => {
          "en" => " minutes",
          "ru" => " минут"
          },
        :hour => {
          "en" => " hours",
          "ru"  => " часов"
          },
        :day => {
          "en" => " days",
          "ru" => " дней"
          },
        :month => {
          "en" => " months",
          "ru" => " месяцев"
          },
        :year => {
          "en" => " years",
          "ru" => " лет"
          },
        :person => {
          "en" => " people",
          "ru" => " человек"
          }
        },
      6 => {
        :minute => {
          "en" => " minutes",
          "ru" => " минут"
          },
        :hour => {
          "en" => " hours",
          "ru"  => " часов"
          },
        :day => {
          "en" => " days",
          "ru" => " дней"
          },
        :month => {
          "en" => " months",
          "ru" => " месяцев"
          },
        :year => {
          "en" => " years",
          "ru" => " лет"
          },
        :person => {
          "en" => " people",
          "ru" => " человек"
          }
        },
      7 => {
        :minute => {
          "en" => " minutes",
          "ru" => " минут"
          },
        :hour => {
          "en" => " hours",
          "ru"  => " часов"
          },
        :day => {
          "en" => " days",
          "ru" => " дней"
          },
        :month => {
          "en" => " months",
          "ru" => " месяцев"
          },
        :year => {
          "en" => " years",
          "ru" => " лет"
          },
        :person => {
          "en" => " people",
          "ru" => " человек"
          }
        },
      8 => {
        :minute => {
          "en" => " minutes",
          "ru" => " минут"
          },
        :hour => {
          "en" => " hours",
          "ru"  => " часов"
          },
        :day => {
          "en" => " days",
          "ru" => " дней"
          },
        :month => {
          "en" => " months",
          "ru" => " месяцев"
          },
        :year => {
          "en" => " years",
          "ru" => " лет"
          },
        :person => {
          "en" => " people",
          "ru" => " человек"
          }
        },
      9 => {
        :minute => {
          "en" => " minutes",
          "ru" => " минут"
          },
        :hour => {
          "en" => " hours",
          "ru"  => " часов"
          },
        :day => {
          "en" => " days",
          "ru" => " дней"
          },
        :month => {
          "en" => " months",
          "ru" => " месяцев"
          },
        :year => {
          "en" => " years",
          "ru" => " лет"
          },
        :person => {
          "en" => " people",
          "ru" => " человек"
          }
        }
      }[number][type][default_lang]
    else
      return "missing"
    end
  end

end
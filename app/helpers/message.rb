# -*- coding: utf-8 -*-

module Message

  def get_last_message
    [
      {
        :id => "2",
        :html => 'Рад приветствовать всех на уютной альфе проекта Eventful! Подробнее о планах, ходе разработки и последних новостях можно узнать в нашем <a href="http://www.yandex.ru">официальном блоге</a>',
      }
    ][-1]
  end

end
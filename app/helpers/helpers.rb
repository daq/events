# -*- coding: utf-8 -*-

#require File.join(File.dirname(__FILE__), "photo.rb")
#require File.join(File.dirname(__FILE__), "video.rb")
require File.join(File.dirname(__FILE__), "time.rb")
#require File.join(File.dirname(__FILE__), "imgur.rb")
require File.join(File.dirname(__FILE__), "translation.rb")
require File.join(File.dirname(__FILE__), "message.rb")

helpers do

  def current_user
    @current_user ||= User[session[:user_id]] if session[:user_id]
  end

  def current_user_name
    @current_user.name if current_user
  end

  def current_user_image
    @current_user.image if current_user
  end

  def default_lang
    if current_user
      current_user.language
      #settings.default_language
    else
      settings.default_language
    end
  end

  def get_tags (str)
    str.gsub(/\s*,\s*/, ',').split(',').map{|item| UnicodeUtils.downcase(item)}
  end

  def bad_string(str)
    return str.nil? || str == "" || str.size > 255
  end

  def bad_text(str)
    return str.nil? || str == "" || str.size > 20480
  end

  def bad_int(str)
    return str.nil? || str == "" || /^\-?\d+\.\d+$/.match(str).nil?
  end

  def to_html (event)
    options = [ autolink: true, no_intra_emphasis: true]
    renderer = Redcarpet::Render::HTML.new(:filter_html => true)
    md = Redcarpet::Markdown.new(renderer, *options )
    md.render(event)
  end

  def get_author_image(event)
    image = User.select(:image).filter(:id => event.author).first.image
    image
  end

  def get_author_name(event)
    name = User.select(:name).filter(:id => event.author).first.name
    name
  end

  #def show_tags (event)
    #'<ul class="pager">' + event.tags_dataset.map { |tag|
      #'<span class="label label-info"><a href="' + url('tag/' + tag[:tag_name]) + '">#' + tag[:tag_name] + '</a></span>'
      #'<li><a href="' + url('tag/' + tag[:tag_name]) + '">#' + tag[:tag_name] + '</a></li>'
    #}.join + '</ul>'
  #end

  def show_user_pics(users)
    users.map{|u| u.image.nil? ? '<img src="' + url('img/avatar.gif') + '" width="32px" height="32px" alt="' + u.name + '"/>' : '<img src="' + u.image + '" width="32px" height="32px" alt="' + u.name + '" />'}.join
  end

  def params_ok(data)
    not (data.nil? || data.include?('') || data.include?(nil))
  end

  def clck(utxt)
    open('http://clck.ru/--?url='+url(utxt)).read
  end

  def get_users_string(users_count)
    if users_count == 1
      return t("person")
    elsif users_count > 4 and users_count < 22
      return t("people")
    else
      num = get_last_digit(users_count)
      return ttime(:person, num)
    end
  end

  #def get_user_visits(users_count)
    #t("go")+'<br><button class="btn btn-large btn-success disabled numbers">'+users_count.to_s+'</button><br>'+get_users_string(users_count)
  #end

  #include VideoHelpers
  #include PhotoHelpers
  include TimeHelpers
  #include Images
  include Translation
  include Message

end
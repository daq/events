module VideoHelpers

  def get_video_provider(video)
    re = /(youtube)\.com|youtube\-nocookie\.com\/|youtu\.be\//
    provider = video.match(re)[1]
    provider
  end

  def get_video_id(video)
    re = /(?:youtube(?:-nocookie)?\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/
    id = video.match(re)[1]
    id
  end

  def get_video_thumbnail(video_id, size = "small")
    if size == "big"
      return "http://img.youtube.com/vi/#{video_id}/0.jpg"
    else
      return "http://img.youtube.com/vi/#{video_id}/2.jpg"
    end
  end

  def add_videos(vd)
    vd.map {|video|
      "<img src='#{video.thumbnail_url}'/>"
    }.join + assign_video_ids(vd)
  end

  def assign_video_ids(vd)
    "<script type='text/javascript' language='JavaScript'>video_objects = {" + vd.map{|video|
      "'#{video.thumbnail_url}': '#{video.vi_id}'"
    }.join(',') + "}</script>"
  end

  class Videor

    attr_reader :link, :video_id, :provider, :thumbnail

    def initialize(link)
      @link = link
      @size = "small"
      get_provider
      get_id
      get_thumbnail
    end

    def get_provider
      re = /(youtube)\.com|youtube\-nocookie\.com\/|youtu\.be\//
      @provider = @link.match(re)[1]
    end

    def get_id
      re = /(?:youtube(?:-nocookie)?\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/
      @video_id = video.match(re)[1]
    end

    def get_thumbnail
      if @size == "big"
        @thumbnail = "http://img.youtube.com/vi/#{@video_id}/0.jpg"
      else
        @thumbnail = "http://img.youtube.com/vi/#{@video_id}/2.jpg"
      end
    end

  end

end
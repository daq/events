# -*- coding: utf-8 -*-

module TimeHelpers  

  def get_server_time(sdate, stime, sgmt_offset)
    Time.parse(sdate + ' ' + stime).utc + Time.new.gmt_offset - sgmt_offset.to_i*3600
  end

  def get_local_time(sdate, stime)
    Time.parse(sdate + ' ' + stime).utc    
  end

  def get_last_digit (num)
    tm = num.modulo(10)
    if tm == 0
      tm
    elsif tm > 9
      get_last_digit(tm)
    else
      tm
    end
  end

  def get_time_string(td)
    res = ""
    if td < 60
      return t("1minute")
    elsif td < 3600
      j = (td/60).floor
      i = get_last_digit(j)
      if j > 20 or j < 10
        puts i, j
        res = j.to_s + ttime(:minute, i)
      else
        res = j.to_s + t("minutes")
      end
    elsif td < 86400
      j = (td/3600).floor
      i = get_last_digit(j)
      if j > 20 or j < 10
        res = j.to_s + ttime(:hour, i)
      else
        res = j.to_s + t("hours")
      end
    else #td < 2592000
      j = (td/86400).floor
      i = get_last_digit(j)
      if (j < 110 and j > 20) or j < 10 or (j < 210 and j > 120) or (j < 310 and j > 220) or (j > 320)
        res = j.to_s + ttime(:day, i)
      else
        res = j.to_s + t("days")
      end
    #else 
    #  j = (td/2592000).floor
    #  i = get_last_digit(j)
    #  if j > 20 or j < 10
    #    res = j.to_s +years[i]
    #  else
    #    res = j.to_s + " лет"
    #  end

    end
  end

  def when_created (created_at)
    get_time_string(Time.now.utc - created_at) + " " + t('ago')
  end

  def when_starts (servertime)
    if (servertime - Time.now.utc) > 0
      #{}"Начнется через " + get_time_string(servertime - Time.now.utc)
      str = get_time_string(servertime - Time.now.utc).split(' ')
      return t("cherez")+'<br><button class="btn btn-large btn-warning disabled numbers">'+str[0]+'</button><br>'+str[1]
    else
      str = get_time_string(Time.now.utc - servertime).split(' ')
      return str[1]+'<br><button class="btn btn-large btn-warning disabled numbers">'+str[0]+'</button><br>'+t("ago")
    end
  end

end
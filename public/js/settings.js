$(document).ready(function() {
  $('#save').live('click', function(){
    $('#save').addClass('disabled');
    $("#label").html("");
    $(".control-group").each(function(i, obj){
      if($(this).hasClass('error')){
        $(this).removeClass('error');
      }
    });

    var joj = {'email': $('#email').val(), 'lang': $('#lang option:selected').text()};
    $.post('/settings', JSON.stringify(joj), function(data){
      var d = JSON.parse(data)
      $("#label").html("");
      if(d['errors']){
        $.each(d['errors'], function(index, value){
          if(!$("#"+value['id']).closest('.control-group').hasClass('error')){
            $("#"+value['id']).closest('.control-group').addClass('error');
          }
          $("#label").append($("<span></span>").addClass("label").addClass("pull-left").addClass("label-warning").text(value['text']));
          $("#label").append("<br/>");
        });
      } else {
        $("#label").html($("<span></span>").addClass("label").addClass("pull-left").addClass("label-success").text("  OK!  "));
      }
      $('#save').removeClass('disabled');
    });
  });
});

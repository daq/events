var myMap;
var collection;
var me_placemark;

function add_me() {
  me_placemark = new ymaps.Placemark([my_coords[0], my_coords[1]], {
    iconContent: "Я"
  }, {
    preset: 'twirl#redIcon'
  });
  myMap.geoObjects.add(me_placemark);
}

function remove_me() {
  myMap.geoObjects.remove(me_placemark);  
}

function query_events(jobj) {
  $.post('/feature_dev', JSON.stringify(jobj), function(data){
    myMap.geoObjects.remove(collection);
    collection.removeAll();
    $.each(data, function(index, value){
      var myPlacemark = new ymaps.Placemark([value["latitude"], value["longitude"]], {
        iconContent: value["title"]
      }, {
        preset: 'twirl#blueStretchyIcon'
      });
      collection.add(myPlacemark);
    });
    myMap.geoObjects.add(collection);
    if(collection.getLength() > 1) {
      myMap.setBounds(collection.getBounds());
    }
  });
}

function get_map_events(ul, lr) {
  var jobj ={ feature: "get_events_by_coords",
              ul_lat: ul[0],
              ul_lng: ul[1],
              lr_lat: lr[0],
              lr_lng: lr[1]
  };
  query_events(jobj);
}

/*function init() {
  // Создание экземпляра карты и его привязка к контейнеру с
  // заданным id ("map")
  myMap = new ymaps.Map('map', {
    // При инициализации карты, обязательно нужно указать
    // ее центр и коэффициент масштабирования
    center: [55.76, 37.64], // Москва
    zoom: 10
  });
  collection = new ymaps.GeoObjectCollection();
  myMap.controls
    // Кнопка изменения масштаба - компактный вариант
    // Расположим её справа
    .add('smallZoomControl', { left: 5, top: 75 })
    // Стандартный набор кнопок
    .add('mapTools');
  bounds = myMap.getBounds();
  var upper_left = [bounds[0][0], bounds[0][1]];
  var lower_right = [bounds[1][0], bounds[1][1]];
  var events = get_map_events(upper_left, lower_right);
}*/


$(document).ready(function() {
  $('#ijoined').live('click', function(){
    query_events({feature: "get_events_i_joined"});
  });
  $('#all_events').live('click', function(){
    query_events({feature: "get_all_events"});
  });
  $('#closest').live('click', function(){
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        function(position){
          var my_coords = [position.coords.latitude, position.coords.longitude]
          query_events({feature: "get_closest_events",
            lat: my_coords[0],
            lng: my_coords[1]
          });
          //add_me();
        },
        function(){
          alert('not supported');
        }
      );
    } else {
      alert('not supported');
    }
  });

});
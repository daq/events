var player;

function addPlayer(th){
  if (typeof player === 'undefined') {
    ;
  } else {
    player.destroy();
  }
  player = new YT.Player('yplayer', {
    height: '360',
    width: '480',
    videoId: video_objects[th]
  });
}

function getScreen(url, size, id)
{
  if(url === null){ return ""; }
  size =(size === null) ? "big" : size;
  if(size == "small"){
    return "<img id='" + id + "' src='http://img.youtube.com/vi/"+url+"/2.jpg'></img>";
  }else {
    return "<img id='" + id + "' src='http://img.youtube.com/vi/"+url+"/0.jpg'></img>";
  }
}

function get_suggested_events(jobj) {
  $.post('/feature_dev', JSON.stringify(jobj), function(data){
    $.each(data, function(index, value){
      $('#sugg')
        .append($('<li></li>')
          .append($('<a></a>').attr('href', '/event/'+value['id']).text(value['title'])));
    });
  });
}

$(document).ready(function(){
  var ev = $('.span8.event-element').attr('id');
  var eventid = ev.substring(2, ev.length);
  get_suggested_events({feature: "get_suggested_events", event_id: eventid});
});
var loading = false;
var end_reached = false;
var jobj = {page: 1};

function wrapEvents(dat){
  var res = $('#result');
  var container = $('<ul class="thumbnails"></ul>');
  var ul = $('<ul class="thumbnails"></ul>');
  var d = $('<div>').html(dat).find('li').each(function(i, value){
    if(i%2 == 0){
      container = $('<ul class="thumbnails"></ul>');
    }
    container.append(value);
    res.append(container);
  });
}

function loadEvents() {
  $.post('/ajx', JSON.stringify(jobj), function(data){
    if (data == "") {
      end_reached = true;
    } else {
      wrapEvents(data);
    };
  });
  loading = false;
  $(".loading").remove();
  return false;
}

$(document).ready(function(){
  loadEvents();
  $('div.alert > a.close').live('click', function(){
    var message_id = $(this).attr('id').split('_')[1]
    var curr_mid = cookie.get('message_id');
    if (curr_mid == null) {
      cookie.set('message_id', message_id, 30);
      //alert('no message_id');
    } else if (parseInt(message_id,10) > parseInt(curr_mid,10)){
      cookie.remove('message_id').set('message_id', message_id, 30);
      //alert('changed message_id');
    } else {
      //alert('same message_id');
    }
  });
  $(window).scroll(function(event){
    var curScroll = $('#result')[0].scrollTop;
    var maxScroll = $('#result')[0].scrollHeight - $('#result').height();
    if(curScroll == maxScroll && loading == false && end_reached == false) {
      loading = true;
      jobj['page'] += 1;
      $('#result').append('<div class="loading"><img src="img/animatedEllipse.gif"></img>Loading...</div>')
      $('#result').scrollTop = $('#result').scrollHeight - $('#result').height();
      loadEvents();
    }
    return false;
  });
  //$('#map-modal').modal(options);
});
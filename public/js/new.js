var p = 1;

function get_tz(coord){
    var geourl = "http://ws.geonames.org/timezone?lat="+coord[0].toString()+"&lng="+coord[1].toString();
    $.ajax({  
      type: "GET",
      url: geourl,
      dataType: "xml",
      success: function(data) {  
        jQuery(data).find('gmtOffset').each(function() {
          var tz = jQuery(this).text();
          $('#gmtoffset').val(tz);
          });
        jQuery(data).find('countryCode').each(function() {
          var cn = jQuery(this).text();
          $('#country').val(cn);
          });
      },
      error: function(error_message) {
        alert('error');
      }
    });
    return false; 
  }

function init() {
  var myMap = new ymaps.Map('map', {
    center: [55.76, 37.64],
    zoom: 10
  });
  myCollection = new ymaps.GeoObjectCollection();

  $('#map_lookup').click(function () {
    var search_query = $('#city').val();

    ymaps.geocode(search_query, {results: 1}).then(function (res) {
      var obb = res.geoObjects.get(0);
      myPlacemark = new ymaps.Placemark(obb.geometry.getCoordinates(), {
        iconContent: search_query
      }, {
          preset: 'twirl#blueStretchyIcon'
      });
      myMap.geoObjects.add(myPlacemark);
        
      myMap.panTo(
        obb.geometry.getCoordinates(), {
          flying: true
      });
      $("#latitude").val(JSON.stringify(obb.geometry.getCoordinates()[0]));
      $("#longitude").val(JSON.stringify(obb.geometry.getCoordinates()[1]));
      get_tz(obb.geometry.getCoordinates());
    });
    return false;
  });
}

$(document).ready(function() {
  $('#datepicker').datepicker();
  $('#save').live('click', function(){
    $("#label").html("");
    var joj = {
      'title': $('#title').val(),
      'city' : $('#city').val(),
      'description': $('#description').val(),
      'latitude': $('#latitude').val(),
      'longitude': $('#longitude').val(),
      'date': $('#timepicker').val(),
      'time': $('#datepicker').val(),
      'gmtoffset': $('#gmtoffset').val(),
      'country': $('#country').val(),
      'tags' : $('#tags').val(),
      'feature': $('#feature').val()
    };
    if(! $('#event_id').length == 0){
      joj['event_id'] = $('#event_id').val();
    }
    $.post('/', JSON.stringify(joj), function(data){
      var d = JSON.parse(data);
      $("#label").html("");
      if(d['errors']){
        $.each(d['errors'], function(index, value){
          if(value['id'] != 'json' && !$("#"+value['id']).closest('.control-group').hasClass('error')){
            $("#"+value['id']).closest('.control-group').addClass('error');
            document.getElementById('new').scrollIntoView();
          }
          $("#label").append($("<span></span>").addClass("label").addClass("pull-left").addClass("label-warning").text(value['text']));
          $("#label").append("<br/>");
        });
      } else {
        $("#label").html($("<span></span>").addClass("label").addClass("pull-left").addClass("label-success").text("  Everything is OK!  "));
        $('#save').addClass('disabled');
        //history.go(-1);
      }
    });
  });
});
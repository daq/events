// Application-wide script

var jobj = {}; // AJAX JSON data object
var event_coords = {};
var event_times = {};
var showme = false;
var eventc;
var map_url = "";

//var user_pics = [];
//var shown_user_pics = 10;

function get_event_id(ref) {
  var b_parent = ref.parents('.event-element');
  var eventid = b_parent.attr('id');
  eventid = eventid.substring(2, eventid.length);
  return eventid;
}

function common_error_message(selector){
  $(selector).append("<b>Could not fetch data</b>");
}

function success(position){
  var lat = position.coords.latitude.toString();
  var lng = position.coords.longitude.toString();
  
  var elat = event_coords[eventc][1];
  var elng = event_coords[eventc][0];
  map_url = 'http://static-maps.yandex.ru/1.x/?l=map&pt='+elng+','+elat+',pm2dgm2~'+lng+','+lat+',pm2rdm1';
  if (showme){
    map_url += '&size=400,300';
  } else {
    map_url += '&z=13&size=400,300';
  }
  //alert(map_url);
  $('#map').html('<center><img src="'+map_url+'" /></center>');
}

function showmap(){
  var elat = event_coords[eventc][1];
  var elng = event_coords[eventc][0];
  var map_url = 'http://static-maps.yandex.ru/1.x/?l=map&pt='+elng+','+elat+',pm2dgm1&z=13&size=400,300';
  if (showme){
    map_url += '&size=400,300';
  } else {
    map_url += '&z=13&size=400,300';
  }
  $('#map').html('<center><img src="'+map_url+'" /></center>');
}

function add_event_coords(eid, arr) {
  event_coords[eid] = arr;
}

function add_event_times(eid, arr) {
  event_times[eid] = arr;
}

function addCalendar(d) {
  //var date = new Date(d[0], d[1], d[2]);
  var year = d[0];
  var month = d[1];
  var day = d[2];
  var wday = new Date(d[0], d[1]-1, 1).getDay();
  if (wday == 0) {
    wday = 7
  }
  var days_in_month = new Date(year, month, 0).getDate();
  var alldays = Math.ceil((days_in_month+wday)/7)*7;
  var html_temp = ""
  for (i = 1; i <= alldays; i++){
    if ((i-1)%7 == 0){
      if (i == 1) {
        html_temp += "<tr>";
      } else if (i == alldays) {
        html_temp += "</tr>";
      } else {
        html_temp += "</tr><tr>";
      }
    }
    if (i < wday || i > (days_in_month+wday-1)) {
      html_temp += "<td></td>";
    } else if (i-wday+1 == day) {
      html_temp += '<td id="day'+(i-wday+1).toString()+'" style="background-color: #ad1d28; color: #fff;"><center><b>'+(i-wday+1).toString()+"</b></center></td>";
    }
     else {
      html_temp += '<td id="day'+(i-wday+1).toString()+'"><center>'+(i-wday+1).toString()+"</center></td>";
    }
  }
  return html_temp;
}


$(document).ready(function() {
  $('#wontgo').live("click", function(){
    var evid = get_event_id($(this));
    var lnk = $(this);
    jobj = {event_id: evid, feature: "remove_user"};
    $.post('/feature', JSON.stringify(jobj), function(data){
      lnk.replaceWith('<input class="btn btn-large btn-success" type="button" id="willgo" value="Присоединиться" />');
    });
    return false; 
  });
  $('#willgo').live("click", function(){
    var evid = get_event_id($(this));
    jobj = {event_id: evid, feature: "add_user"};
    var lnk = $(this);
    $.post('/feature', JSON.stringify(jobj), function(data){
      lnk.replaceWith('<input class="btn btn-large btn-danger" type="button" id="wontgo" value="Отказаться" />');
    });
    return false; 
  });
  /*$('.btn-mini.btn-info').live('click', function(){
    $('#map-modal > .modal-footer > #bigmap').attr('href', '/map/'+get_event_id());
  });*/

  var url = window.location.pathname.split('/')
  var last = url.pop(-1);
  var prelast = url[url.length - 1];
  
  if(prelast == 'tag'){
    jobj['tag'] = last;
    $('li#timeline').addClass('active');
    jobj['feature'] = 'timeline';
  } else if(last == "map" || prelast == "map") {
    $('li#map_page').addClass('active');
  } else if(prelast == "user"){
    jobj['feature'] = "user_events";
    jobj['user_id'] = last;
  } else {
    jobj['feature'] = last;
    $('li#' + last).addClass('active');
  }

  if($('span#n_total').val() > 0){
    $('span#n_total').addClass('badge-warning');
  }
  if($('span#notifications').val() > 0){
    $('span#notifications').addClass('badge-success');
  }
  $('#mm').live('click', function(){
    showme = false;
    eventc = get_event_id($(this));
    showmap();
  });
  $('#cm').live('click', function(){
    $('#cbody').html("");
    eventc = get_event_id($(this));
    $('#cbody').html(addCalendar(event_times[eventc]));
    //$('h3#hdf').text('Календарь событий 22');
  });
  $('#showme').live('click', function(){
    showme = true;
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(success, showmap);
    } else {
      alert('not supported');
    }
  });

});